library(ggplot2)
library(tikzDevice)
options(tikzPdftexWarnUTF = F)
options(tikzDocumentDeclaration = "\\documentclass{beamer}")

# norm.from <- -2
# norm.to   <- 4
# norm.out  <- 20
# norm.step <- (norm.to - norm.from)/norm.out
# norm.points <- seq(norm.from, norm.to, length.out = norm.out)
# norm.hist <- data.frame(x=norm.points,
#                         y=pmax(dnorm(norm.points + norm.step/2, 1, 0.9),
#                                dnorm(norm.points - norm.step/2, 1, 0.9)))

# ggplot(norm.hist, aes(x=x)) +
#   theme(axis.title = element_blank()) + 
#   stat_function(fun=dnorm, args = list(mean=1, sd=0.9), geom="line") +
#   geom_linerange(aes(ymax=y), ymin=0, linetype=2) +
#   geom_segment(data=head(norm.hist, ceiling(norm.out/2)),
#                aes(x=x, xend=x+norm.step, y=y, yend=y), linetype = 2) +
#   geom_point(data=head(norm.hist, ceiling(norm.out/2)),
#                aes(x=x+norm.step/2, y=y)) +
#   geom_segment(data=tail(norm.hist, floor(norm.out/2)),
#                aes(x=x, xend=x-norm.step, y=y, yend=y), linetype = 2) +
#   geom_point(data=tail(norm.hist, floor(norm.out/2)),
#              aes(x=x-norm.step/2, y=y))

coord <- coord_cartesian(xlim=c(-2, 4), ylim=c(0, 0.5))

lim.low <- -2
lim.high <- 4
steps <- 20

tikz("normaldensity1.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  coord
dev.off()

tikz("normaldensity2.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  annotate(geom="point", x=2, y=dnorm(2, 1, 0.9)) +
  coord
dev.off()

tikz("normaldensity3.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  annotate(geom="segment", x=1.8, xend=1.8, y=0, yend=dnorm(2, 1, 0.9), linetype=2) +
  annotate(geom="segment", x=2.2, xend=2.2, y=0, yend=dnorm(2, 1, 0.9), linetype=2) +
  annotate(geom="segment", x=1.8, xend=2.2, y=dnorm(2, 1, 0.9), yend=dnorm(2, 1, 0.9)) +
  annotate(geom="point", x=2, y=dnorm(2, 1, 0.9)) +
  coord
dev.off()

tikz("normaldensity4.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x+(lim.high-lim.low)/(2*steps), mean=1,sd=0.9), geom="step", linetype=2, n=steps) +
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  coord
dev.off()


steps <- 20
bars <- data.frame(x = seq(-2, 4, 6/19)[13:15] + 3/19)
bars$y <- dnorm(bars$x, 1, 0.9)
tikz("normaldensity5.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x+(lim.high-lim.low)/(2*steps), mean=1,sd=0.9), geom="step", linetype=2, n=steps) +
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  geom_vline(xintercept=c(1.7894, 2.7368), colour="red") +
  geom_bar(stat="identity", data=bars, aes(x=x, y=y), width=6/19, alpha=0.9, fill="red") +
  coord
dev.off()

steps <- 60
bars <- data.frame(x = seq(-2, 4, 6/(steps-1))[38:47] + 3/(steps-1))
bars$y <- dnorm(bars$x, 1, 0.9)
tikz("normaldensity6.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x+(lim.high-lim.low)/(2*steps), mean=1,sd=0.9), geom="step", linetype=2, n=steps) +
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  geom_vline(xintercept=c(1.7894, 2.7368), colour="red") +
  geom_bar(stat="identity", data=bars, aes(x=x, y=y), width=6/(steps-1), alpha=0.9, fill="red") +
  coord
dev.off()

tikz("normaldensity7.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) + 
  stat_function(xlim=c(1.7894, 2.7368), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=c(1.7894, 2.7368), colour="red") +
  coord
dev.off()

tikz("normaldensity8.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low, lim.high)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) + 
  stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord
dev.off()

tikz("normaldensities1.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low-1, lim.high+1)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm"))) +
  stat_function(fun=function(x) dnorm(x, mean=0.3,sd=0.9), geom="line", linetype=1, colour="red") +
  annotate(geom="segment", x=0.3, xend=0.3, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm")), colour="red") +
  coord
dev.off()

tikz("normaldensities2.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low-1, lim.high+1)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm"))) +
#  stat_function(fun=function(x) dnorm(x, mean=0.3,sd=0.9), geom="line", linetype=1, colour="red") +
#  annotate(geom="segment", x=0.3, xend=0.3, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm")), colour="red") +
  stat_function(fun=function(x) dnorm(x, mean=1,sd=1.4), geom="line", linetype=1, colour="blue") +
  annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 1.4), arrow=arrow(unit(20, "cm")), colour="blue") +
  # stat_function(fun=function(x) dnorm(x, mean=1,sd=1.6), geom="line", linetype=1, colour="blue") +
  # annotate(geom="segment", x=1, xend=1, y=0, yend=dnorm(1, 1, 0.9)) +
  # annotate(geom="segment", x=1, xend=1.9, y=dnorm(1.9, 1, 0.9), yend=dnorm(1.9, 1, 0.9)) +
  coord
dev.off()

tikz("normaldensities3.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low-1, lim.high+1)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  #annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm"))) +
  #  stat_function(fun=function(x) dnorm(x, mean=0.3,sd=0.9), geom="line", linetype=1, colour="red") +
  #  annotate(geom="segment", x=0.3, xend=0.3, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm")), colour="red") +
  stat_function(fun=function(x) dnorm(x, mean=1,sd=1.4), geom="line", linetype=1, colour="blue") +
  #annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 1), arrow=arrow(unit(20, "cm")), colour="blue") +
  # stat_function(fun=function(x) dnorm(x, mean=1,sd=1.6), geom="line", linetype=1, colour="blue") +
  annotate(geom="segment", x=1-0.9, xend=1+0.9, y=dnorm(1+0.9, 1, 0.9), yend=dnorm(1+0.9, 1, 0.9), arrow=arrow(ends = 'both', length = unit(0.75, "cm"))) +
  annotate(geom="segment", x=1-1.4, xend=1+1.4, y=dnorm(1+1.4, 1, 1.4), yend=dnorm(1+1.4, 1, 1.4), colour="blue", arrow=arrow(ends = 'both', length = unit(0.75, "cm"))) +
    # annotate(geom="segment", x=1, xend=1.9, y=dnorm(1.9, 1, 0.9), yend=dnorm(1.9, 1, 0.9)) +
  coord
dev.off()

tikz("normaldensities4.tex", width = 3, height = 2)
ggplot(data.frame(x=c(lim.low-1, lim.high+1)), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  #annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm"))) +
  stat_function(fun=function(x) dnorm(x, mean=0.3,sd=0.9), geom="line", linetype=1, colour="red") +
  #  annotate(geom="segment", x=0.3, xend=0.3, yend=0, y=dnorm(1, 1, 0.9), arrow=arrow(unit(20, "cm")), colour="red") +
  stat_function(fun=function(x) dnorm(x, mean=1,sd=1.4), geom="line", linetype=1, colour="blue") +
  #annotate(geom="segment", x=1, xend=1, yend=0, y=dnorm(1, 1, 1), arrow=arrow(unit(20, "cm")), colour="blue") +
  # stat_function(fun=function(x) dnorm(x, mean=1,sd=1.6), geom="line", linetype=1, colour="blue") +
  #annotate(geom="segment", x=1-0.9, xend=1+0.9, y=dnorm(1+0.9, 1, 0.9), yend=dnorm(1+0.9, 1, 0.9), arrow=arrow(ends = 'both', length = unit(0.75, "cm"))) +
  #annotate(geom="segment", x=1-1.4, xend=1+1.4, y=dnorm(1+1.4, 1, 1.4), yend=dnorm(1+1.4, 1, 1.4), colour="blue", arrow=arrow(ends = 'both', length = unit(0.75, "cm"))) +
  # annotate(geom="segment", x=1, xend=1.9, y=dnorm(1.9, 1, 0.9), yend=dnorm(1.9, 1, 0.9)) +
  coord
dev.off()


tikz("normalhypothesis1a.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
#  stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
#  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
#  stat_function(fun=function(x) dnorm(x, mean=-0.5,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1b.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=qnorm(c(0.175, 0.225), 1, 0.9), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=qnorm(c(0.175, 0.225), 1, 0.9), colour="red") +
  #  stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
#  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
#  stat_function(fun=function(x) dnorm(x, mean=-0.5,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1c.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=qnorm(c(0.175, 0.225), 1, 0.9), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=qnorm(c(0.175, 0.225), 1, 0.9), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=-0.5,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=qnorm(c(0.175, 0.225), 1, 0.9), fun=function(x) dnorm(x, mean=-0.5,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1d.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=c(-5, qnorm(0.05, 1, 0.9)), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=-0.5,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=-0.5,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1e.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=c(-5, qnorm(0.05, 1, 0.9)), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=0.2,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=0.2,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1f.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=c(-5, qnorm(0.05, 1, 0.9)), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=0.7,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=0.7,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1g.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=c(-5, qnorm(0.05, 1, 0.9)), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=2,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(-5, qnorm(0.05, 1, 0.9)), fun=function(x) dnorm(x, mean=2,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1h.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(qnorm(0.95, 1, 0.9), 7), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=qnorm(0.95, 1, 0.9), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=2,sd=0.9), geom="line", linetype=1, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(qnorm(0.95, 1, 0.9), 7), fun=function(x) dnorm(x, mean=2,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1i.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(qnorm(0.95, 1, 0.9), 7), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  geom_vline(xintercept=qnorm(0.95, 1, 0.9), colour="red") +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=2,sd=0.9), geom="line", linetype=3, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(qnorm(0.95, 1, 0.9), 7), fun=function(x) dnorm(x, mean=2,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=0,sd=0.9), geom="line", linetype=3, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(qnorm(0.95, 1, 0.9), 7), fun=function(x) dnorm(x, mean=0,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="blue", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()

tikz("normalhypothesis1j.tex", width = 3, height = 2)
ggplot(data.frame(x=c(-6, 8), fac='Nollhypotes'), aes(x=x)) +
  theme(axis.title = element_blank()) + 
  stat_function(fun=function(x) dnorm(x, mean=1,sd=0.9), geom="line", linetype=1) +
  stat_function(xlim=c(qnorm(0.975, 1, 0.9), 7), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(xlim=c(-5, qnorm(0.025, 1, 0.9)), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  # stat_function(xlim=c(1 + 0.9*1.96, 4), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  #  stat_function(xlim=c(-2, 1 - 0.9*1.96), fun=function(x) dnorm(x, mean=1,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=2,sd=0.9), geom="line", linetype=3, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(qnorm(0.975, 1, 0.9), 7), fun=function(x) dnorm(x, mean=2,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  stat_function(fun=function(x) dnorm(x, mean=0,sd=0.9), geom="line", linetype=3, data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.')) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(qnorm(0.975, 1, 0.9), 7), fun=function(x) dnorm(x, mean=0,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="blue", alpha=0.9) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(-5, qnorm(0.025, 1, 0.9)), fun=function(x) dnorm(x, mean=0,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="blue", alpha=0.9) +
  stat_function(data=data.frame(x=c(lim.low, lim.high), fac='Alternativhyp.'), xlim=c(-5, qnorm(0.025, 1, 0.9)), fun=function(x) dnorm(x, mean=2,sd=0.9), geom="ribbon", aes(ymin=0, ymax=..y..), fill="red", alpha=0.9) +
  coord_cartesian(xlim=c(-4, 6)) + facet_grid(fac~.)
dev.off()
